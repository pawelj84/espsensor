#include <Arduino.h>
#include <WiFiUdp.h>

void send_udp_data(WiFiUDP *fudp, const char *address, int port, char *buff, size_t buff_len)
{
    uint8_t *buffer = (uint8_t *)calloc(buff_len, sizeof(uint8_t));
    memcpy(buffer, buff, buff_len);
    //send hello world to server
    fudp->beginPacket(address, port);
    fudp->write(buffer, buff_len);
    fudp->endPacket();
    memset(buffer, 0, buff_len);
    //processing incoming packet, must be called before reading the buffer
    fudp->parsePacket();
    //receive response from server, it will be HELLO WORLD
    if (fudp->read(buffer, 50) > 0)
    {
        Serial.print("Server to client: ");
        Serial.println((char *)buffer);
    }
    free(buffer);
}

void generate_json(char **data, const char *name, size_t *jlen, int data1, int data2)
{
    char json[100] = "";
    sprintf(json, "{\"sensor\":\"%s\",\"temp1\":%i,\"temp2\":%i}", name, data1, data2);
    *jlen = strlen(json);
    *data = (char *)malloc(*jlen + 1);
    memcpy(*data, json, *jlen);
}