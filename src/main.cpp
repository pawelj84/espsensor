#include <Arduino.h>
#include <WiFi.h>
#include <WiFiUdp.h>

#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include "netlib.h"

const char *ssid = "SSID";
const char *password = "password";

const char *udpAddress = "192.168.1.255"; //send to broadcast address
const int udpPort = 44444;                //set default port

const char *SensorName = "Sensor1"; //friendly name for the board

uint8_t sensor_1_pin = 23; // pin sensor 1
uint8_t sensor_2_pin = 10; // pin sensor 2

WiFiUDP udp;

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
#define OLED_RESET -1 // Reset pin # (or -1 if sharing Arduino reset pin)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

void setup()
{
  Serial.begin(9600);
  WiFi.begin(ssid, password);
  // put your setup code here, to run once:
  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  if (!display.begin(SSD1306_SWITCHCAPVCC, 0x3C))
  {
    Serial.println(F("SSD1306 allocation failed"));
    for (;;)
      ; // Don't proceed, loop forever
  }

  display.display();

  udp.begin(udpPort);
  randomSeed(analogRead(0));
}

void loop()
{
  char *json_string = NULL;
  size_t jstr_len = 0;

  uint16_t temp1 = 0;
  uint16_t temp2 = 0;
  temp1 = random(65536);
  temp2 = random(65536);
  generate_json(&json_string, SensorName, &jstr_len, temp1, temp2);
  send_udp_data(&udp, udpAddress, udpPort, json_string, jstr_len);

  Serial.println(jstr_len);
  Serial.println(json_string);
  free(json_string);
  delay(1000);
}
